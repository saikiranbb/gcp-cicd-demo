module "project-factory" {
  source  = "terraform-google-modules/project-factory/google"
  version = "~> 10.1"
  #source  = "app.terraform.io/saikiranbb/project/google"
  #version = "1.0.1"

  name            = var.project_id
  org_id          = "700281068730"
  billing_account = "017402-CD8BFE-074133"
  activate_apis   = ["compute.googleapis.com"]
}
